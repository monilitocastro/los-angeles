import React, { Component } from 'react';
import Header from './header';

export default class App extends Component {
  render() {
    return (
      <div>
        <div className='row'>
          <div className='container'>
            <Header />
          </div>
        </div>
        <div className='row'>
          {this.props.children}
        </div>
      </div>
    );
  }
}
