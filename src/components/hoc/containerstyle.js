import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

export default function( OriginalComponent ){
    class Composition extends Component{
        render(){
            return (
                <div className='container'>
                    <OriginalComponent {...this.props} />
                </div>
            );
        }
    }

    return Composition;

}
