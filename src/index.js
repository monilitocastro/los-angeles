/*import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import './index.css';

// ReactDOM.render(<App />, document.getElementById('root'));

const rootEl = document.getElementById('root');
 
ReactDOM.render(
    <AppContainer>
        <App />
    </AppContainer>,
  rootEl
);

if (module.hot) {
    module.hot.accept('./App', () => {
        const NextApp = require('./App').default; // eslint-disable-line global-require
        ReactDOM.render(
            <AppContainer>
                <NextApp />
            </AppContainer>,
            rootEl
        );
    });
}*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';
import { AppContainer } from 'react-hot-loader';

import registerServiceWorker from './registerServiceWorker';
import App from './components/app';
import Signin from './components/auth/signin';
import Signup from './components/auth/signup';
import Signout from './components/auth/signout';
import Feature from './components/feature';

//  Import HOC
import RequireAuth from './components/hoc/requireauth';
import ClearError from './components/hoc/clearerror';
import ContainerStyle from './components/hoc/containerstyle';
import { UPDATE_AUTH } from './actions/types';

import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem('token');

if(token){
  store.dispatch({ type: UPDATE_AUTH, payload: token !== null });
}

const rootEl = document.getElementById('root');

//  Compose HOC components
const SigninHOC = ContainerStyle(ClearError(Signin));
const SignupHOC = ContainerStyle(ClearError(Signup));
const SignoutHOC = ContainerStyle(Signout);
const FeatureHOC = ContainerStyle(RequireAuth(Feature));

ReactDOM.render(
    <AppContainer>
        <Provider store={store}>
            <Router history={ browserHistory }>
            <Route path="/" component={App} >
                <Route path="signin" component={ SigninHOC } />
                <Route path="signup" component={ SignupHOC } />
                <Route path="signout" component={SignoutHOC} />
                <Route path="feature" component={ FeatureHOC  } />
            </Route>
            </Router>
        </Provider>
    </AppContainer>
  , rootEl);


if (module.hot) {
    module.hot.accept('./components/app', () => {
        const NextApp = require('./components/app').default; // eslint-disable-line global-require
        ReactDOM.render(
            <AppContainer>
                <Provider store={store}>
                    <Router history={ browserHistory }>
                    <Route path="/" component={NextApp} >
                        <Route path="signin" component={ ClearError(Signin) } />
                        <Route path="signup" component={ ClearError(Signup) } />
                        <Route path="signout" component={Signout} />
                        <Route path="feature" component={RequireAuth(Feature)} />
                    </Route>
                    </Router>
                </Provider>
            </AppContainer>,
            rootEl
        );
    });
}

registerServiceWorker();
